#include <iostream>

#include <vector>

#include "module/factory/factory.h"
#include "module/modules/read/di/di.h"
#include "module/modules/read/dio/dio.h"
#include "module/modules/write/di/di.h"
#include "module/modules/write/dio/dio.h"

template <class ModuleBase>
void test_module_type(const module::factory::arguments_t& args)
{
    using ModuleBasePtr = std::shared_ptr<ModuleBase>;

    std::vector<ModuleBasePtr> modules;
    modules.push_back(ModuleBasePtr(module::factory::Factory<ModuleBase>::GetInstance().Create("DIO", args)));
    modules.push_back(ModuleBasePtr(module::factory::Factory<ModuleBase>::GetInstance().Create("DI", args)));
    try
    {
        modules.push_back(ModuleBasePtr(module::factory::Factory<ModuleBase>::GetInstance().Create("DO", args)));
    }
    catch (const std::exception& e)
    {
        std::cout << e.what() << std::endl;
    }

    for (auto module : modules)
    {
        module->Print();
    }
}

int main()
{
    module::factory::arguments_t args;
    args.index = 1;
    args.system_facade = 2;
    args.options = 3;

    std::cout << "\tTesting reading modules" << std::endl;
    test_module_type<module::modules::read::Base>(args);
    
    std::cout << "\tTesting writing modules" << std::endl;
    test_module_type<module::modules::write::Base>(args);
}
