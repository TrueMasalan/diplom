#pragma once

#include "arguments.h"

namespace module
{
    
namespace factory
{

template <class ModuleBase>
class Creator
{
private:
    using create_method_t = ModuleBase* (*)(const arguments_t& args);

public:
    Creator(const module::name_t& name, const create_method_t& create_method)
        : m_module_name(name), m_create_method(create_method)
    {

    }

public:
    const module::name_t& GetModuleName() const
    {
        return m_module_name;
    }

    ModuleBase* Create(const arguments_t& args) const
    {
        return m_create_method(args);
    }

private:
    const module::name_t m_module_name;
    const create_method_t m_create_method;
};

} // namespace factory

} // namespace module
