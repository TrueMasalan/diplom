#pragma once 

namespace module
{
    
namespace factory
{

struct arguments_t
{
    int index;
    int system_facade;
    int options;
};

} // namespace factory

} // namespace module
