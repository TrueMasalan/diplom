#pragma once

#include <list>
#include <stdexcept>
#include <iostream>

#include "module/name.h"
#include "creator.h"

namespace module
{

namespace factory
{

template <class ModuleBase>
class Creators
{
public:
    static Creators& GetInstance()
    {
        static Creators<ModuleBase> creators;
        return creators;
    }

private:
    using creators_t = std::list<Creator<ModuleBase>>;
    
public:
    void AddCreator(const Creator<ModuleBase>& creator)
    {
        for (typename creators_t::const_iterator cit = m_creators.begin(); cit != m_creators.end(); ++cit)
        {
            const name_t& module_name = creator.GetModuleName();
            if (cit->GetModuleName() == module_name)
            {
                std::cout << "Module " << module_name 
                    << " creator already exists - not adding" << std::endl;
                return;
            }
        }
        m_creators.push_back(creator);
    }

    const Creator<ModuleBase>& GetCreator(const module::name_t& module_name)
    {
        for (auto& creator : m_creators)
        {
            if (creator.GetModuleName() == module_name)
            {
                return creator;
            }
        }
        throw std::logic_error("unregistered module");
    }

private:
    Creators()
    {

    };

    creators_t m_creators;
};

} // namespace factory

} // namespace module