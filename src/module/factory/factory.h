#pragma once

#include <sstream>
#include <stdexcept>

#include "module/name.h"
#include "module/factory/creators.h"

namespace module
{

namespace factory
{

template <class ModuleBase>
class Factory
{
public:
    static Factory& GetInstance()
    {
        static Factory<ModuleBase> factory;
        return factory;
    }

    ModuleBase* Create(const name_t& module_name, const arguments_t& args)
    {
        try
        {
            const Creator<ModuleBase>& creator 
                = Creators<ModuleBase>::GetInstance().GetCreator(module_name);
            return creator.Create(args);
        }
        catch (const std::exception& e)
        {
            std::ostringstream ostream;
            ostream << "Error while creating module " << module_name << ": " << e.what();
            throw std::logic_error(ostream.str());
        }
    }   

private:
    Factory() 
    {

    };
};

} // namespace factory

} // namespace module