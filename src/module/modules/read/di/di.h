#pragma once

#include "module/modules/read/base/base.h"

namespace module
{

namespace modules
{

namespace read
{    

class DI : public Base
{
public:
    DI(int a);

    virtual ~DI();

    virtual void Print() override;

private:
    int arg1;
};

} // namespace read

} // namespace modules

} // namespace module
