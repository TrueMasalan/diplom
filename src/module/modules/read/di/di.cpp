#include "di.h"

#include <iostream>

#include "module/registrant.h"

using namespace module::modules::read;

DI::DI(int a) : arg1(a)
{

}

DI::~DI()
{

}

void DI::Print()
{
    std::cout << "DI " << arg1 << std::endl;
}

REGISTER_MODULE(Base, DI)
{
    return new DI(args.index);
}
