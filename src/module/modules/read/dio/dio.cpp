#include "dio.h"

#include <iostream>

#include "module/registrant.h"

using namespace module::modules::read;

DIO::DIO(int a, int b) : arg1(a), arg2(b)
{

}

DIO::~DIO()
{

}

void DIO::Print()
{
    std::cout << "DIO " << arg1 << " " << arg2 << std::endl;
}

REGISTER_MODULE(Base, DIO)
{
    return new DIO(args.index, args.system_facade);
}