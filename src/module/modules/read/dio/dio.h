#pragma once 

#include "module/modules/read/base/base.h"

namespace module
{

namespace modules
{

namespace read
{    

class DIO : public Base
{
public:
    DIO(int a, int b);

    virtual ~DIO();

    virtual void Print() override;

private:
    int arg1, arg2;
};

} // namespace read

} // namespace modules

} // namespace module
