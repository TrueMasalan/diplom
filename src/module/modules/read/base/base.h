#pragma once

#include <string>
#include <memory>

namespace module
{

namespace modules
{

namespace read
{    

class Base
{
public:
    Base();

    virtual ~Base();

    virtual void Print() {};
};

} // namespace read

} // namespace modules

} // namespace module
