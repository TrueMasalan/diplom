#pragma once

#include <string>
#include <memory>

namespace module
{

namespace modules
{

namespace write
{    

class Base
{
public:
    Base();

    virtual ~Base();

    virtual void Print() {};
};

} // namespace write

} // namespace modules

} // namespace module
