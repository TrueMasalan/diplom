#pragma once 

#include "module/modules/write/base/base.h"

namespace module
{

namespace modules
{

namespace write
{

class DIO : public Base
{
public:
    DIO(int a, int b);

    virtual ~DIO();

    virtual void Print() override;

private:
    int arg1, arg2;
};

} // namespace write

} // namespace modules

} // namespace module
