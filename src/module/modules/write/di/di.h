#pragma once

#include "module/modules/write/base/base.h"

namespace module
{

namespace modules
{

namespace write
{    

class DI : public Base
{
public:
    DI(int a);

    virtual ~DI();

    virtual void Print() override;

private:
    int arg1;
};

} // namespace write

} // namespace modules

} // namespace module
