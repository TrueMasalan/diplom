#pragma once

#include "module/factory/creators.h"

namespace module
{

template <class ModuleBase>
class Registrant
{
public:
    Registrant(const factory::Creator<ModuleBase>& creator)
    {
        factory::Creators<ModuleBase>::GetInstance().AddCreator(creator);
    }
};

#define REGISTER_MODULE(module_base, module_class) \
    static module_base* module_class##CreateMethodAdapter(const module::factory::arguments_t& args); \
    static module::factory::Creator<module_base> module_class##Creator(#module_class, module_class##CreateMethodAdapter); \
    \
    static module::Registrant<module_base> module_class##Registrant(module_class##Creator); \
    \
    static module_base* module_class##CreateMethodAdapter(const module::factory::arguments_t& args)

} // namespace module
