#pragma once

#include <string>

namespace module
{
    
using name_t = std::string;

} // namespace module
